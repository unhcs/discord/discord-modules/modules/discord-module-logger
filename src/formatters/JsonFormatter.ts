import { MappedObject } from "utils/FilterUtil";

const JsonFormatter = (object: MappedObject) => {
    return JSON.stringify(object, null, 4);
};

export default JsonFormatter;
