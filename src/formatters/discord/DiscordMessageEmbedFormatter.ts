import { EmbedBuilder } from "@discordjs/builders";
import { FileOptions, MessageAttachment, MessageOptions } from "discord.js";
import { MappedObject } from "utils/FilterUtil";

const DiscordMessageEmbedFormatter = (object: MappedObject) => {
    let files: FileOptions[] = object.attachments.map(function (
        a: MessageAttachment
    ) {
        return {
            attachment: a.attachment,
            name: a.name
        } as FileOptions;
    });

    const description: string = object.message;

    if (description.length > 4000) {
        files = [
            ...files,
            {
                attachment: Buffer.from(object.message),
                name: object.referenceId + ".txt"
            }
        ];
    }

    return {
        files: files,
        embeds: [
            new EmbedBuilder()
                .setDescription(
                    description.length > 4000 || description.length == 0
                        ? null
                        : description
                )
                .setTimestamp(new Date())
                .setFooter({
                    text: object.referenceId
                }).data
        ]
    } as MessageOptions;
};

export default DiscordMessageEmbedFormatter;
