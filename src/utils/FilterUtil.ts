declare type MappedObject = { [index: string]: any };

function filter(object: MappedObject, whitelist: string[]): MappedObject {
    return whitelist.reduce((obj, key) => ({ ...obj, [key]: object[key] }), {});
}

export { filter, MappedObject };
