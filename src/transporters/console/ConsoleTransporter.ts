import Transporter from "transporters/Transporter";
import { Formatter } from "formatters/Formatter";
import { MappedObject } from "utils/FilterUtil";

interface ConsoleTransporterOptions {
    formatter: Formatter;
}

class ConsoleTransporter extends Transporter {
    constructor(options: ConsoleTransporterOptions) {
        super(options);
    }

    public async transport(
        originalObject: MappedObject,
        refinedObject: MappedObject
    ): Promise<void> {
        console.log(this.formatter(refinedObject));
    }
}

export default ConsoleTransporter;
