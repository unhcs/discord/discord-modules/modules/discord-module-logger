import { model, Model, Schema } from "mongoose";
import MessageMetaMongoDBTransporter, { IMessageMeta, MessageMetaMongoDBTransporterOptions, MessageMetaSchemaBase } from "./MessageMetaMongoDBTransporter";

interface IMessageDelete extends IMessageMeta {
    deletedAt: Date;
}

const MessageDeleteSchema: Schema = new Schema({
    ...MessageMetaSchemaBase,
    deletedAt: {
        type: Date,
        required: true
    }
});

const MessageDelete: Model<IMessageDelete> = model(
    "MessageDelete",
    MessageDeleteSchema
);


class MessageDeleteMongoDBTransporter extends MessageMetaMongoDBTransporter {
    constructor(options: MessageMetaMongoDBTransporterOptions) {
        super(options, MessageDelete);
    }
}

export default MessageDeleteMongoDBTransporter;
