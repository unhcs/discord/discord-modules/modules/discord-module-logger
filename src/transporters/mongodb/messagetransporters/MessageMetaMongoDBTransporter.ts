import { Module } from "@unh-csonline/discord-modules";
import { Document, model, Model, Schema } from "mongoose";
import MongoDBTransporter from "transporters/mongodb/messagetransporters/MongoDBTransporter";

interface IMessageMeta extends Document {
    referenceId: string;
    id: string;
    guild: {
        id: string | null;
        name: string | null;
    };
    channel: {
        id: string;
        name: string | null;
        category: {
            id: string;
            name: string;
        } | null;
    };
    author: {
        id: string;
        username: string;
        nickname: string | null;
    };
    type: string;
    mentions: {
        everyone: boolean;
        members: [
            {
                id: string;
                username: string;
                nickname: string | null;
            }
        ];
        roles: [
            {
                id: string;
                name: string;
            }
        ];
        channels: [
            {
                id: string;
                name: string | null;
                category: {
                    id: string;
                    name: string;
                } | null;
            }
        ];
    };
}

const MessageMetaSchemaBase = {
    referenceId: {
        type: String,
        required: true
    },
    id: {
        type: String,
        required: true
    },
    guild: {
        id: {
            type: String,
            required: false
        },
        name: {
            type: String,
            required: false
        }
    },
    channel: {
        id: {
            type: String,
            required: true
        },
        name: {
            type: String,
            required: false
        },
        parent: {
            id: {
                type: String,
                required: false
            },
            name: {
                type: String,
                required: false
            }
        }
    },
    author: {
        id: {
            type: String,
            required: true
        },
        username: {
            type: String,
            required: true
        },
        nickname: {
            type: String,
            required: false
        }
    },
    type: {
        type: String,
        required: true
    },
    mentions: {
        everyone: {
            type: Boolean,
            required: true
        },
        members: [
            {
                id: {
                    type: String,
                    required: true
                },
                username: {
                    type: String,
                    required: true
                },
                nickname: {
                    type: String,
                    required: false
                }
            }
        ],
        roles: [
            {
                id: {
                    type: String,
                    required: true
                },
                name: {
                    type: String,
                    required: true
                }
            }
        ],
        channels: [
            {
                id: {
                    type: String,
                    required: true
                },
                name: {
                    type: String,
                    required: false
                },
                parent: {
                    id: {
                        type: String,
                        required: false
                    },
                    name: {
                        type: String,
                        required: false
                    }
                }
            }
        ]
    }
}

interface MessageMetaMongoDBTransporterOptions {
    module: Module;
}

export default abstract class MessageMetaMongoDBTransporter extends MongoDBTransporter {
    constructor(options: MessageMetaMongoDBTransporterOptions, model: Model<any>) {
        super({
            module: options.module,
            model: model
        });
    }
}

export {
    IMessageMeta,
    MessageMetaSchemaBase,
    MessageMetaMongoDBTransporterOptions
};
