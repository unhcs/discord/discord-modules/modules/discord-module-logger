import { Formatter } from "formatters/Formatter";
import ObjectFormatter from "formatters/ObjectFormatter";
import { MappedObject } from "utils/FilterUtil";

interface TransporterOptions {
    formatter?: Formatter;
}

abstract class Transporter {
    public readonly formatter: Formatter;

    protected constructor(options?: TransporterOptions) {
        this.formatter = options?.formatter ?? ObjectFormatter;
    }

    public abstract transport(
        originalObject: MappedObject,
        refinedObject: MappedObject
    ): Promise<void>;
}

export default Transporter;

export { TransporterOptions };
