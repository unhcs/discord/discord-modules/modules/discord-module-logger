import { Module } from "@unh-csonline/discord-modules";
import MessageCreateMetaFilter from "filters/messagefilters/MessageCreateMetaFilter";
import MessageMetadataTransform from "transforms/messagetransforms/MessageMetadataTransform";
import Logger, { LoggerOptions, LoggerType } from "loggers/Logger";
import MessageCreateMetaMongoDBTransporter from "transporters/mongodb/messagetransporters/MessageCreateMongoDBTransporter";

class BaseMessageMetaLogger extends Logger {
    constructor(module: Module) {
        const options: LoggerOptions = {
            module: module,
            type: LoggerType.MessageCreateEvent,
            filter: new MessageCreateMetaFilter(),
            transforms: [new MessageMetadataTransform()],
            transporters: [
                new MessageCreateMetaMongoDBTransporter({
                    module: module
                })
            ]
        };

        super(options);
    }
}

export default BaseMessageMetaLogger;
