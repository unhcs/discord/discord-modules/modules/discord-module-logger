import { Module } from "@unh-csonline/discord-modules";
import MemberJoinFilter from "filters/memberfilters/MemberJoinFilter";
import MemberJoinTransform from "transforms/membertransforms/MemberJoinTransform";
import MemberJoinMongoDBTransporter from "transporters/mongodb/membertransporters/MemberJoinMongoDBTransporter";
import Logger, { LoggerOptions, LoggerType } from "loggers/Logger";

class MemberJoinLogger extends Logger {
    constructor(module: Module) {
        const options: LoggerOptions = {
            module: module,
            type: LoggerType.MemberJoinEvent,
            filter: new MemberJoinFilter(),
            transforms: [new MemberJoinTransform()],
            transporters: [
                new MemberJoinMongoDBTransporter({
                    module: module,
                })
            ]
        };

        super(options);
    }
}

export default MemberJoinLogger;
