import { Module } from "@unh-csonline/discord-modules";
import MemberRemoveFilter from "filters/memberfilters/MemberRemoveFilter";

import Logger, { LoggerOptions, LoggerType } from "loggers/Logger";
import MemberRemoveTransform from "transforms/membertransforms/MemberRemoveTransform";
import MemberRemoveMongoDBTransporter from "transporters/mongodb/membertransporters/MemberRemoveMongoDBTransporter";

class MemberRemoveLogger extends Logger {
    constructor(module: Module) {
        const options: LoggerOptions = {
            module: module,
            type: LoggerType.MemberRemoveEvent,
            filter: new MemberRemoveFilter(),
            transforms: [new MemberRemoveTransform()],
            transporters: [
                new MemberRemoveMongoDBTransporter({
                    module: module,
                })
            ]
        };

        super(options);
    }
}

export default MemberRemoveLogger;
