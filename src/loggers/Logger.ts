import { Module } from "@unh-csonline/discord-modules";
import Filter from "filters/Filter";
import JsonFormatter from "formatters/JsonFormatter";
import Transform from "transforms/Transform";
import ConsoleTransporter from "transporters/console/ConsoleTransporter";
import Transporter from "transporters/Transporter";
import { MappedObject } from "utils/FilterUtil";
import { ReferenceId, ReferenceIdType } from "utils/ReferenceIdUtil";

enum LoggerType {
    MessageCreateEvent,
    MessageUpdateEvent,
    MemberJoinEvent,
    MessageDeleteEvent,
    MemberRemoveEvent
}

interface LoggerOptions {
    module: Module;
    type: LoggerType;
    filter?: Filter;
    transforms?: Transform[];
    transporters?: Transporter[];
    referenceId?: ReferenceIdType;
}

abstract class Logger {
    public readonly module: Module;
    public readonly type: LoggerType;
    public readonly transforms: Transform[];
    public readonly filter?: Filter;
    public readonly transporters: Transporter[];

    protected constructor(options: LoggerOptions) {
        this.module = options.module;
        this.type = options.type;
        this.transforms = options.transforms ?? [];
        this.filter = options.filter;
        this.transporters = options.transporters ?? [];

        if (this.transporters == []) {
            this.transporters = [
                new ConsoleTransporter({
                    formatter: JsonFormatter
                })
            ];
        }
    }

    public log(
        object: MappedObject,
        referenceId: ReferenceIdType = ReferenceId()
    ): void {
        let refinedObject = object;

        /**
         * Transformations are applied in the order they are specified. Each transform mutates the object in the
         *      previous transform.
         * A transform should exist to concrete all desired properties. Doing so avoids potential issues when the
         *      discord.js API changes.
         */
        for (const transform of this.transforms) {
            refinedObject = transform.apply(refinedObject);
        }

        /**
         * The filter is used to remove any undesired properties from the transformed object. All required properties
         *      can be omitted from filters as they are appended to the object post-filter.
         */
        if (this.filter) {
            refinedObject = this.filter.apply(refinedObject);
        }

        /**
         * All log entries have a reference id property. This property can be used to link entries in different
         *      transport streams.
         * A transport-global id can be specified by passing it as an option during construction. If no id is passed,
         *      a transport-specific id is generated.
         */
        refinedObject = {
            referenceId: referenceId,
            ...refinedObject
        };

        /**
         * Transporters are the output streams for log entries. If no transporter is specified, the ConsoleTransporter
         *      will be used.
         */
        for (const transporter of this.transporters) {
            transporter.transport(object, refinedObject).catch(console.error);
        }
    }
}

export default Logger;

export { LoggerType, LoggerOptions };
