import { Module } from "@unh-csonline/discord-modules";
import { Message, PartialMessage } from "discord.js";
import { LoggerType } from "loggers/Logger";
import LoggerRepository from "repositories/LoggerRepository";
import LoggingListener from "../LoggingListener";

export default class MessageDeleteListener extends LoggingListener<"messageDelete"> {
    constructor(module: Module, loggerRepository: LoggerRepository) {
        super(module, loggerRepository, LoggerType.MessageDeleteEvent, "messageDelete");
    }

    async execute(message: Message<boolean> | PartialMessage): Promise<void> {
        this.log(message)
    }
}