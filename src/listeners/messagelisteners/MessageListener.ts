import { Module } from "@unh-csonline/discord-modules";
import { Message } from "discord.js";
import { LoggerType } from "loggers/Logger";
import LoggerRepository from "repositories/LoggerRepository";
import LoggingListener from "../LoggingListener";

export default class MessageListener extends LoggingListener<"messageCreate"> {
    constructor(module: Module, loggerRepository: LoggerRepository){
        super(module, loggerRepository, LoggerType.MessageCreateEvent, "messageCreate");
    }

    async execute(message: Message<boolean>): Promise<void> {
        if (message.author.bot) {
            return Promise.resolve();
        }

        await this.log(message).catch(console.error);
    }
}
