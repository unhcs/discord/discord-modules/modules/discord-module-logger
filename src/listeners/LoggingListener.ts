import { Listener, Module } from "@unh-csonline/discord-modules";
import { ClientEvents } from "discord.js";
import { LoggerType } from "loggers/Logger";
import LoggerRepository from "repositories/LoggerRepository";
import { MappedObject } from "utils/FilterUtil";
import { ReferenceId, ReferenceIdType } from "utils/ReferenceIdUtil";

export default abstract class LoggingListener<K extends keyof ClientEvents> extends Listener<K>{
    readonly loggerRepository: LoggerRepository;
    private loggerType: LoggerType;

    constructor(module: Module, loggerRepository: LoggerRepository, loggerType: LoggerType, eType: K){
        super(module, eType);
        this.loggerRepository = loggerRepository;
        this.loggerType = loggerType;
    }

    protected log(obj: MappedObject) {
        const referenceId: ReferenceIdType = ReferenceId();

        for( const logger of this.loggerRepository.getAllOfType(
            this.loggerType
        )) {
            try{
                logger.log(obj, referenceId);
            } catch (err) {
                console.error(err);
            }
        }

        return Promise.resolve();
    }
}