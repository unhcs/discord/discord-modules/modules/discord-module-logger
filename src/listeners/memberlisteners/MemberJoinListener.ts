import { Module } from "@unh-csonline/discord-modules";
import { GuildMember } from "discord.js";
import { LoggerType } from "loggers/Logger";
import LoggerRepository from "repositories/LoggerRepository";
import LoggingListener from "../LoggingListener";

export default class MemberJoinListener extends LoggingListener<"guildMemberAdd"> {
    constructor(module: Module, loggerRepository: LoggerRepository) {
        super(module,loggerRepository, LoggerType.MemberJoinEvent, "guildMemberAdd");
    }

    async execute(member: GuildMember): Promise<void> {
        await this.log(member).catch(console.error);
    }
}