import { Module } from "@unh-csonline/discord-modules";
import { GuildMember } from "discord.js";
import { LoggerType } from "loggers/Logger";
import LoggerRepository from "repositories/LoggerRepository";
import LoggingListener from "../LoggingListener";

export default class MemberRemoveListener extends LoggingListener<"guildMemberRemove"> {
    constructor(module: Module, loggerRepository: LoggerRepository){
        super(module, loggerRepository, LoggerType.MemberRemoveEvent, "guildMemberRemove");
    }

    async execute(member: GuildMember ): Promise<void> {
        this.log(member).catch(console.error);
    }
}