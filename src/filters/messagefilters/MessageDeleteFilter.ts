import MessageMetaFilter from "./MessageMetaFilter";

class MessageDeleteFilter extends MessageMetaFilter {
    constructor() {
        super(["deletedAt"]);
    }
}

export default MessageDeleteFilter;
