import Filter, { FilterOptions } from "filters/Filter";

class MessageContentFilter extends Filter {
    constructor() {
        const options: FilterOptions = {
            keys: ["message", "attachments"]
        };

        super(options);
    }
}

export default MessageContentFilter;
