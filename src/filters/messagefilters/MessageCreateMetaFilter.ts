import MessageMetaFilter from "./MessageMetaFilter";

class MessageCreateMetaFilter extends MessageMetaFilter {
    constructor() {
        super(["createdAt"]);
    }
}

export default MessageCreateMetaFilter;
