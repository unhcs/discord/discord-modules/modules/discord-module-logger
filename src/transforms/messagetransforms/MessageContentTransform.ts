import Transform from "transforms/Transform";
import { MappedObject } from "utils/FilterUtil";
import { MessageAttachment } from "discord.js";

class MessageContentTransform extends Transform {
    public apply(object: MappedObject): MappedObject {
        return {
            message: object["cleanContent"],
            attachments: object["attachments"].map((a: MessageAttachment) => a)
        };
    }
}

export default MessageContentTransform;
