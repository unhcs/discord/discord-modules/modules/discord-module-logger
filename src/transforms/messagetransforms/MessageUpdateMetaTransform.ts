
import { MappedObject } from "utils/FilterUtil";

import Transform from "transforms/Transform";

class MessageUpdateTransform extends Transform {
    public apply(object: MappedObject): MappedObject {
        const transformedProperties = {
            editedAt: object.editedAt
        };

        return { ...object, ...transformedProperties };
    }
}

export default MessageUpdateTransform;
