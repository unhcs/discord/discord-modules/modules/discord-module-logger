import { MappedObject } from "utils/FilterUtil";

import MemberTransform from "./MemberTransform";

class MemberRemoveTransform extends MemberTransform {
    public override apply(object: MappedObject): MappedObject {
        const obj = super.apply(object);

        return { ...obj, removedAt: new Date() };
    }
}

export default MemberRemoveTransform
