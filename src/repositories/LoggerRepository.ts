import Logger, { LoggerType } from "loggers/Logger";

/**
 * Stores registered loggers. Listeners reference this repository to fetch the relevant loggers by type. Loggers are
 *      stored by type to optimize for retrieval.
 */
class LoggerRepository {
    private loggers: Map<LoggerType, Logger[]>;

    constructor() {
        this.loggers = new Map();
    }

    /**
     * Add a logger to the repository. Stored in arrays based on type for fast lookup.
     *
     * @param logger
     */
    public add(logger: Logger): void {
        this.loggers.set(logger.type, [
            ...(this.loggers.get(logger.type) ?? []),
            logger
        ]);
    }

    /**
     * Used to fetch the array of loggers of a specific type.
     *
     * @param type
     * @returns {Logger[]}
     */
    public getAllOfType(type: LoggerType): Logger[] {
        return this.loggers.get(type) ?? [];
    }
}

export default LoggerRepository;
