# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## 1.0.0 (2022-03-15)

### Features

-   add basic transporter implementation ([cd14fc2](https://gitlab.com/unh-cs-department/discord/discord-modules/modules/discord-module-logger/commit/cd14fc294571695896e371c85f8ffe725943cb15))
-   implement basic iteration of event-triggered data logging ([70741a9](https://gitlab.com/unh-cs-department/discord/discord-modules/modules/discord-module-logger/commit/70741a94a46960290eaeeca6b0cd0ebd40198e18))
-   implement guild-relative Discord transporter ([00061fa](https://gitlab.com/unh-cs-department/discord/discord-modules/modules/discord-module-logger/commit/00061fac3adc5a9c7a7a9319883b3c58fd3c6687))
-   implement message content and meta logging ([aff1c5e](https://gitlab.com/unh-cs-department/discord/discord-modules/modules/discord-module-logger/commit/aff1c5e66a2fa0abb6435e33f156379cf1995f2b))

### Bug Fixes

-   enforce requirement of username, not nickname, in message meta transporter ([2493854](https://gitlab.com/unh-cs-department/discord/discord-modules/modules/discord-module-logger/commit/249385403b55193a046a7ab40be0e003e40f4ea4))
-   prettier formatting of unsupported file extensions ([570fa2c](https://gitlab.com/unh-cs-department/discord/discord-modules/modules/discord-module-logger/commit/570fa2c50e0f4d8d090ae3d25d2a32d2cc454452))
-   update name of DiscordMessageEmbedFormatter export to match definition ([4eff8c4](https://gitlab.com/unh-cs-department/discord/discord-modules/modules/discord-module-logger/commit/4eff8c425c5acdb41c412b406cdf7bc1cbb64d63))
